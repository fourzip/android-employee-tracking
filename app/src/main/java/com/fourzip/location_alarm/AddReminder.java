package com.fourzip.location_alarm;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by NAISA_JOHN on 1/18/2017.
 */

public class AddReminder extends LeftMenusMediaActivity{

    FrameLayout frameLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.add_reminder);
        frameLayout = (FrameLayout)findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.add_reminder,frameLayout);



        TextView textView = (TextView) findViewById(R.id.reminder_text);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        textView.setTypeface(typeface);

        Button button = (Button) findViewById(R.id.task_list);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddReminder.this, TaskActivity.class);
                startActivity(intent);
            }
        });

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }

        //Button create_reminder_fab = (Button) findViewById(R.id.create_reminder);
        //create_reminder_fab.setOnClickListener(new View.OnClickListener() {
    }
             public void onCreateNewTask(View v)
        {

                AlertDialog.Builder builder=new AlertDialog.Builder(AddReminder.this);
                builder.setMessage(R.string.dialog_message_task)
                        .setTitle(R.string.dialog_title_task);



                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        //jump to add reminder or fetch reminder activity
                        dialog.cancel();
                        Intent intent=new Intent(AddReminder.this,CreateNewTask.class);
                        startActivity(intent);

                    }
                });

                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }



}
