package com.fourzip.location_alarm;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by NAISA_JOHN on 11/21/2016.
 */
public class TabNewTask extends Fragment {
    Context context;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_view, container, false);
        final ListView listView=(ListView)rootView.findViewById(R.id.list_view);

        //in the empty parameter, we have to pass the result we fetch from json api

        AsyncHttpClient client=new AsyncHttpClient();

        Toast.makeText(getContext(),"outside" , Toast.LENGTH_SHORT).show();
        client.get("http://api.androidhive.info/contacts/",new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                //super.onSuccess(statusCode, headers, response);
                Toast.makeText(getActivity(), "Successfully connected", Toast.LENGTH_LONG).show();

                ArrayList<String> items = new ArrayList<String>();
                try {
                    JSONObject data = response;
                    JSONArray contacts = data.getJSONArray("contacts");
                    for (int i = 0; i < contacts.length(); i++) {

                        JSONObject data1 = contacts.getJSONObject(i);
                        String names = data1.getString("name");
                        items.add(names);

                    }
                    ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.fragment_newtask, R.id.List_data, items);
                    listView.setAdapter(mArrayAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

                @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getActivity(),"Error in Connection",Toast.LENGTH_LONG).show();
            }

    });
        return rootView;
    }

}
