package com.fourzip.location_alarm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by NAISA_JOHN on 2/1/2017.
 */

public class parallax_activity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parallax_view);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Parallax Activity");
        setSupportActionBar(toolbar);

        final ListView listView=(ListView)findViewById(R.id.list_view);


       // ArrayAdapter<String> mAdapter=new ArrayAdapter<String >(R.layout.list_data,);


        final AsyncHttpClient client=new AsyncHttpClient();

        client.get("http://api.androidhive.info/contacts/",new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
               // super.onSuccess(statusCode, headers, response);
                Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_LONG).show();
                ArrayList<String> items=new ArrayList<>();

                try
                {
                    JSONObject data=response;
                    JSONArray contacts = data.getJSONArray("contacts");
                    for(int i=0;i<contacts.length();i++) {

                        JSONObject data1 = contacts.getJSONObject(i);
                        String names = data1.getString("name");
                        items.add(names);
                    }
                    ArrayAdapter<String> mArrayAdapter=new ArrayAdapter<String>(getApplicationContext(),R.layout.list_data,items);
                    listView.setAdapter(mArrayAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }
}
