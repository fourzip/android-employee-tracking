package com.fourzip.location_alarm;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by NAISA_JOHN on 1/18/2017.
 */

public class CreateAccount extends  AppCompatActivity  {

    Context context;
    DialogInterface dialogInterface;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_details);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Step 2:");
        setSupportActionBar(toolbar);

        Button onBack=(Button)findViewById(R.id.detail_backbtn);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CreateAccount.this,MainActivity.class);
                startActivity(intent);
            }
        });


    }
}
