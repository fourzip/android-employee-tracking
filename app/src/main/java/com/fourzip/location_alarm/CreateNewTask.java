package com.fourzip.location_alarm;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
//import android.util.Calendar;
import java.text.DateFormat;
import java.util.Calendar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import java.util.Date;
import java.util.Locale;
import java.util.Timer;

/**
 * Created by NAISA_JOHN on 1/19/2017.
 */
//@TargetApi(24)
public class CreateNewTask extends AppCompatActivity {


    final Calendar calendar = Calendar.getInstance(Locale.US);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_task);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Create Ur Task Here");
        setSupportActionBar(toolbar);

        ImageButton add_new_location=(ImageButton) findViewById(R.id.add_location);
        add_new_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent google_maps_Activity=new Intent(CreateNewTask.this,MapsActivity.class);
                startActivity(google_maps_Activity);
            }
        });

        //Button reference
        Button submit_button=(Button)findViewById(R.id.submit);

        //Selecting time edittext reference
        final EditText time_txt = (EditText) findViewById(R.id.select_time);


        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        // final Calendar calendar = Calendar.getInstance(Locale.US);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };


        ImageButton button = (ImageButton) findViewById(R.id.calendar_icon);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CreateNewTask.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        //TimerPickerDialog instance


        time_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                final TimePickerDialog timePickerDialog;
                timePickerDialog=new TimePickerDialog(CreateNewTask.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        time_txt.setText(hourOfDay + ":" + minute );
                    }
                },hour,minute,true);
                timePickerDialog.setTitle("Select Time");
                timePickerDialog.show();
            }

        });
            submit_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(CreateNewTask.this,Display_Created_Task.class);
                    startActivity(intent);
                    }
            });
    }

        /*TimePickerDialog.OnTimeSetListener time= new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            }
        };

            }*/


        //@TargetApi(24)

    public void updateLabel()
        {
            String myFormat = "MM/dd/yy"; //In which you need put here
            //android.text.format.DateFormat(myFormat);

           // DateFormat df=new DateFormat(myFormat,Locale.US);
           /* SimpleDateFormat sdf= null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                sdf = new SimpleDateFormat(myFormat, Locale.US);
            }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                disp_date.setText(sdf.format(calendar.getTime()));
            }*/

            EditText disp_date=(EditText)findViewById(R.id.date_text);
            Date date=calendar.getTime();
            disp_date.setText(date.toString());
        }

}
