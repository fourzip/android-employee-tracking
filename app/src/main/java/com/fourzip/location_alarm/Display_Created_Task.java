package com.fourzip.location_alarm;

import android.app.LauncherActivity;
import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fourzip.location_alarm.adapter.CardViewAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.fourzip.location_alarm.model.RowItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by NAISA_JOHN on 2/9/2017.
 */

public class Display_Created_Task extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ConnectionManager cm;
    ListView listView;
    ArrayList<RowItem> card_Data;
    DatabaseHelper db;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view_data);
        //setContentView(R.layout.list_view);
        //listView = (ListView) findViewById(R.id.list_view);

        TextView textView = (TextView) findViewById(R.id.jsondata);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view1);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setHasFixedSize(true);
        card_Data=new ArrayList<>();




        //recyclerView.setAdapter(new RowItem());

    //This code was wriiten to check wether the json data is fetched or not
        //final ArrayList <String> dataItems = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.androidhive.info/contacts/", new JsonHttpResponseHandler() {



            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                //super.onSuccess(statusCode, headers, response);
                JSONObject data = response;
                try {

                    JSONArray contacts = data.getJSONArray("contacts");
                    for (int i = 0; i < contacts.length(); i++) {

                        JSONObject data1 = contacts.getJSONObject(i);
                       RowItem items =new RowItem(data1.getString("name"),data1.getString("id"),data1.getString("email"));
                        //dataItems.add(names);
                        //dataItems.add(id);
                        //dataItems.add(email);

                        card_Data.add(items);
                    }
                     //ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_data, dataItems);
                    //listView.setAdapter(mArrayAdapter);
                    mAdapter=new CardViewAdapter(card_Data);
                    recyclerView.setAdapter(mAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
            }
        });

    }



}
