package com.fourzip.location_alarm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fourzip.location_alarm.R;
import com.fourzip.location_alarm.model.DummyModel;
//import com.csform.android.uiapptemplate.util.DummyContent;

import java.util.ArrayList;
import java.util.List;

public class DrawerMediaAdapter extends BaseAdapter {
	
	private Context mContext;
	private List<DummyModel> mDrawerItems;
	private LayoutInflater mInflater;

	public DrawerMediaAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mDrawerItems = getDrawerMediaDummyList();
		mContext = context;
	}

	public static ArrayList<DummyModel> getDrawerMediaDummyList() {
		ArrayList<DummyModel> list = new ArrayList<>();
		list.add(new DummyModel(0, "", "New Tasks", R.string.material_icon_headphones));
		list.add(new DummyModel(1, "", "Upcoming Tasks", R.string.material_icon_human));
		list.add(new DummyModel(2, "", "Completed Tasks", R.string.material_icon_headline));
		list.add(new DummyModel(3, "", "Edit Tasks", R.string.material_icon_soundcloud));
		list.add(new DummyModel(4, "", "Settings", R.string.material_icon_statistics));
		return list;
	}
	@Override
	public int getCount() {
		return mDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return mDrawerItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(
					R.layout.list_view_item_navigation_drawer_media, parent,
					false);
			holder = new ViewHolder();
			holder.icon = (TextView) convertView
					.findViewById(R.id.list_item_navigation_drawer_media_icon);
			holder.title = (TextView) convertView.findViewById(R.id.list_item_navigation_drawer_media_title);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}


		DummyModel item = mDrawerItems.get(position);
		holder.icon.setText(mContext.getString(item.getIconRes()));
		holder.title.setText(item.getText());

		return convertView;
	}

	private static class ViewHolder {
		public TextView icon;
		public/* Roboto */TextView title;
	}
}
