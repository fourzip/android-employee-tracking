package com.fourzip.location_alarm.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.fourzip.location_alarm.R;
import com.fourzip.location_alarm.model.RowItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NAISA_JOHN on 2/22/2017.
 */

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.MyViewHolder> {

    ArrayList<RowItem> dataSet;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout, parent, false);
        MyViewHolder obj=new MyViewHolder(view);
        return obj;
    }
    public CardViewAdapter(ArrayList<RowItem> mDataSet)
    {
        this.dataSet=mDataSet;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        EditText task_name;
        EditText task_date;
        EditText task_time;

        public MyViewHolder(View itemView) {
            super(itemView);
             task_name = (EditText) itemView.findViewById(R.id.task_name_data);
             task_date = ( EditText) itemView.findViewById(R.id.task_date_data);
             task_time = ( EditText) itemView.findViewById(R.id.task_time_data);
        }

    }
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.task_name.setText(dataSet.get(position).getTask_name());
            holder.task_date.setText(dataSet.get(position).getTask_date());
            holder.task_time.setText(dataSet.get(position).getTask_time());
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }



}
