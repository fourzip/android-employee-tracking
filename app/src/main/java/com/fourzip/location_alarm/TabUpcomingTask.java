package com.fourzip.location_alarm;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by NAISA_JOHN on 11/21/2016.
 */
public class TabUpcomingTask extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.parallax_view, container, false);
        Toolbar toolbar=(Toolbar)rootView.findViewById(R.id.toolbar);
        toolbar.setTitle("Parallax Activity");

        ((AppCompatActivity) getActivity()).setSupportActionBar( toolbar);
        final TextView task_name=(TextView)rootView.findViewById(R.id.task_name);
        final TextView task_detail=(TextView)rootView.findViewById(R.id.task_details);
        final TextView task_detail_data=(TextView)rootView.findViewById(R.id.details);
        final TextView task_date=(TextView)rootView.findViewById(R.id.task_date);
        final TextView task_time=(TextView)rootView.findViewById(R.id.task_time);

        //final ListView listView=(ListView)rootView.findViewById(R.id.list_view);

        final AsyncHttpClient client=new AsyncHttpClient();

        client.get("http://api.androidhive.info/contacts/",new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // super.onSuccess(statusCode, headers, response);
                Toast.makeText(getActivity(),"Success",Toast.LENGTH_LONG).show();
                ArrayList<String> names_array=new ArrayList<>();
                ArrayList<String> id_array=new ArrayList<>();
                ArrayList<String> email_array=new ArrayList<>();


                try
                {
                    JSONObject data=response;
                    JSONArray contacts = data.getJSONArray("contacts");
                    //for(int i=0;i<contacts.length();i++) {

                        JSONObject get_data = contacts.getJSONObject(1);
                        String names = get_data.getString("name");
                        names_array.add(names);
                        task_name.setText(names_array +"");


                    JSONObject get_data1 = contacts.getJSONObject(2);
                        String id=get_data1.getString("id");
                        id_array.add(id);
                        task_detail.setText(id_array + "");

                    JSONObject get_data2 = contacts.getJSONObject(3);
                        String email=get_data2.getString("email");
                        email_array.add(email);
                        task_detail_data.setText(email_array + "");

                    task_date.setText(email_array + "");
                    task_time.setText(id_array + "");

                    //}
                    //ArrayAdapter<String> mArrayAdapter=new ArrayAdapter<String>(getActivity(),R.layout.list_data,items);
                    //listView.setAdapter(mArrayAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
        return rootView;
    }


}
