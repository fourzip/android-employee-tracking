package com.fourzip.location_alarm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.fourzip.location_alarm.View.FloatingTextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    DatabaseHelper myDB;
    FloatingTextView username;
    FloatingTextView password;
    TextView register_Txt;
    Button signOut;
    String name, pass;
    //step 1;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static String TAG="Auhentication Message";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        //step 2;
        // initialize the FirebaseAuth instance and the AuthStateListener method so you can track whenever the user signs in or out.
        mAuth=FirebaseAuth.getInstance();

        //Listening for auth state.


        signOut=(Button)findViewById(R.id.Sign_out);





        //The constructor will  be called and database will be created.
        myDB=new DatabaseHelper(this);
       // myDB.open();
        //frameLayout = (FrameLayout)findViewById(R.id.content_frame);
        //getLayoutInflater().inflate(R.layout.activity_login_dark,frameLayout);

        //get references of username and password textfields
         username=(FloatingTextView) findViewById(R.id.username_txt);
         password=(FloatingTextView) findViewById(R.id.password_txt);
       // final EditText usernm_error=(EditText)findViewById(R.id.name_error_msg) ;
        //final EditText password_error=(EditText)findViewById(R.id.password_error_msg);
        register_Txt=(TextView)findViewById(R.id.register);
        clearTextFields();

        mAuth.signOut();

// this listener will be called when there is change in firebase user session
        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(MainActivity.this, AddReminder.class));
                    finish();
                }
            }
        };

        /*ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }*/

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                SignOut();
               /* mAuthListener= new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user != null) {

                            Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                        } else {
                            Toast.makeText(getApplicationContext(),"Signed Out",Toast.LENGTH_LONG).show();
                            Log.d(TAG, "onAuthStateChanged:signed_out:");
                        }
                    }
                };*/


            }
        });

        Button Login_btn=(Button)findViewById(R.id.Login);
        Login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name=username.getText().toString();
                pass=password.getText().toString();
                mAuth.signInWithEmailAndPassword(name,pass).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if(!task.isSuccessful())
                        {
                            Toast.makeText(getApplicationContext(),getString(R.string.Auth_failed),Toast.LENGTH_LONG).show();
                        }
                        else{
                            startActivity(new Intent(MainActivity.this,AddReminder.class));
                        }

                    }
                });

                //Prev, Get locaiton page was displayed
                //Intent intent=new Intent(MainActivity.this,GetLocation.class);
                //startActivity(intent);


               /* if(username.getText().toString().equals("Jerine") && password.getText().toString().equals("John")) {
                    Intent intent = new Intent(MainActivity.this, AddReminder.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Invalid Login",Toast.LENGTH_LONG).show();
                }*/

                //Firebase auth method to login the user with the valid EmailId n password



                if(username.getText().length()==0)
                {
                    username.setError("Field cannot be left blank");
                    //Toast.makeText(getApplicationContext(),"You cant leave Username empty",Toast.LENGTH_LONG).show();
                    return;
                }
                if(password.getText().length()==0)
                {
                    password.setError("Field cannot be left blank");
                    //Toast.makeText(getApplicationContext(),"You cant leave Password empty",Toast.LENGTH_LONG).show();
                    return;
                }
                if(username.getText().length()==0 && password.getText().length()==0)
                {
                    username.setError("Field cannot be left blank");
                    password.setError("Field cannot be left blank");
                    return;
                }
                //Insert data into database
                boolean isInserted= myDB.insertData(username.getText().toString(),password.getText().toString());
                if(isInserted==true)
                {
                    //Toast.makeText(getApplicationContext(),"Successfully Inserted",Toast.LENGTH_LONG).show();
                    return;
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Error while Insertion",Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });

        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    if (username.getText().length() == 0) {
                        // code to execute when EditText loses focus
                        username.setError("Field cannot be left blank");
                        //Toast.makeText(getApplicationContext(),"You cant leave Username empty",Toast.LENGTH_LONG).show();
                        return;
                    }
                }
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    if (password.getText().length() == 0) {
                        password.setError("Field cannot be left blank");
                        return;
                    }
                }
            }
        });


        //Facebook stetho instance
        //create an initializer builder
        Stetho.InitializerBuilder initializerBuilder=Stetho.newInitializerBuilder(this);
        //Enable chrome dev tools
        initializerBuilder.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this));

        //Enable command line interface
        initializerBuilder.enableDumpapp(Stetho.defaultDumperPluginsProvider(this));

        //use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer=initializerBuilder.build();

        //Initialize Stetho with the Initializer
        Stetho.initialize(initializer);

    }

public void SignOut()
{
    mAuth.signOut();
}
    @Override
    protected void onStart() {
        super.onStart();
        //mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        clearTextFields();
    }

    public void onRegister(View v)
        {
            Intent intent=new Intent(MainActivity.this,RegisterUser.class);
            startActivity(intent);
        }

        public void onFabClick(View v)
        {
            Intent intent=new Intent(MainActivity.this,AddReminder.class);
            startActivity(intent);
        }

    public void clearTextFields()
    {
        username.setText("");
        password.setText("");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //close database connection
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    ProgressDialog mProgressDialog;

        public void showProgressDialog() {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage(getString(R.string.loading));
                mProgressDialog.setIndeterminate(true);
            }

            mProgressDialog.show();
        }

        public void hideProgressDialog() {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

