package com.fourzip.location_alarm.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by NAISA_JOHN on 9/29/2016.
 */
public class MaterialDesignIconTextView extends TextView {

    private static Typeface sMaterialDesign ;
    public MaterialDesignIconTextView(Context context) {
        this(context,null);
    }

    public MaterialDesignIconTextView(Context context, AttributeSet attrs) {
       this(context, attrs,0);
    }

    public MaterialDesignIconTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(isInEditMode())
            return;
        setTypeface();
    }

    private void setTypeface()
    {
    if(sMaterialDesign==null)
    {
        sMaterialDesign=Typeface.createFromAsset(getContext().getAssets(),"fonts/MaterialDesignIcons.ttf");
    }
        setTypeface(sMaterialDesign);
    }
}
