package com.fourzip.location_alarm;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

/**
 * Created by NAISA_JOHN on 2/1/2017.
 */

public class dbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME="Reminder_DB";
    public static final String DATABASE_TABLE="Login_data";
    public static final int DATABASE_VERSION=1;
    SQLiteDatabase db;
    public static final String COLUMN_NAME="USERNAME";
    public static final String COLUMN_PASSWORD="PASSWORD";


    //Whenever this constructor is created the database is created
    public dbHelper(Context context)  {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //just to check whether the database and table is created or not
        //db=this.getWritableDatabase();
    }

    //Called when you first create your database
    @Override
    public void onCreate(SQLiteDatabase db) {
       /* db.execSQL("create table " +DATABASE_TABLE
                + "( " + " ID INTEGER PRIMARY KEY AUTOINCREMENT, " //ID
                + COLUMN_NAME + " TEXT NOT NULL, "  //Name field
                + COLUMN_PASSWORD + " TEXT NOT NULL );"  );  //Password field*/
        String sql="Create table " + DATABASE_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,USERNAME TEXT,PASSWORD TEXT);";
        db.execSQL(sql);
        Log.v("SQL",sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + DATABASE_TABLE);
        onCreate(db);
    }
    public dbHelper open() throws SQLException {
        this.db = getWritableDatabase();
        return this;
    }
    //Create insert data method
    public boolean insertData(String name, String password)
    {
       SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COLUMN_NAME,name);
        contentValues.put(COLUMN_PASSWORD,password);
        long result=db.insert(DATABASE_TABLE,null,contentValues);
        if(result==-1)
        {
            return false;
        }
        else{
        return true;}
    }

}
