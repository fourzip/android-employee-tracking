package com.fourzip.location_alarm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by NAISA_JOHN on 2/2/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME="MYDATABASE.db";
    public static final String DATABASE_TABLE="LOGIN_TABLE";
    public static final String COL_1="Username";
    public static final String COL_2="Password";
    public static final int DATABASE_VERSION=2;

    //Table 2 for Location_updates
    public static String DATABASE_TABLE2="Location_updates";
    public static String COL_LAT="Latitude";
    public static String COL_LONG="Longitude";
    public static String TIME="Timestamp";
    SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DATABASE_TABLE + "( " + " ID INTEGER PRIMARY KEY AUTOINCREMENT, Username TEXT, Password TEXT)");
        db.execSQL("create table " + DATABASE_TABLE2 + "( " + " ID INTEGER PRIMARY KEY AUTOINCREMENT, Latitude TEXT, Longitude TEXT, Timestamp DATETIME)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE2);
        onCreate(db);
    }
    public boolean insertData(String name, String password)
    {
        db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(COL_1,name);
        values.put(COL_2,password);
        long result=db.insert(DATABASE_TABLE,null,values);
        if(result==-1)
        {
        return false;}
        else
            return true;
    }
    public boolean insertLocationData(String Latitude, String Longitude, String Timestamp)

    {
        db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(COL_LAT,Latitude);
        values.put(COL_LONG,Longitude);
        values.put(TIME,Timestamp);
        long res=db.insert(DATABASE_TABLE2,null,values);
        if(res==-1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Cursor getData()
    {
        db=this.getWritableDatabase();
        Cursor res=db.rawQuery("Select * from" + DATABASE_TABLE2,null);
        return res;
    }

    //Close database connection
    public void close()
    {
        db.close();
    }
}
