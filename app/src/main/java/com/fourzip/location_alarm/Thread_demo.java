package com.fourzip.location_alarm;
import android.content.EntityIterator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



import java.util.Timer;

/**
 * Created by NAISA_JOHN on 2/7/2017.
 */

public class Thread_demo extends AppCompatActivity implements Runnable {

    GetLocation obj;

    public static String data="";
    Handler mHandler;
    //handler is used to communicate with the UI thread.
    /*private Thread_demo()

    {
        //defines a handler object that is attached to the UI thread.
        mHandler=new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };


    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thread_demo);
        final EditText set_text=(EditText) findViewById(R.id.thread_data);
        Button button=(Button)findViewById(R.id.thread_button);
        final TextView get_text=(TextView)findViewById(R.id.get_thread_data);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        data=set_text.getText().toString();

                        //use View.post method to manipulate the UI when using thread
                        get_text.post(new Runnable() {
                            @Override
                            public void run() {
                                get_text.setText(data);
                            }
                        });
                    }
                }).start();
            }
        });
    }



    public void run()
    {
        //Moves the current thread into the background
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);


    }
}
