package com.fourzip.location_alarm;

/**
 * Created by
 * NAISA_JOHN on 1/19/2017.
 */
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import android.text.format.Time;
import android.widget.Toast;

import static com.loopj.android.http.AsyncHttpClient.log;

public class GetLocation extends AppCompatActivity {
    //implements ConnectionCallbacks, OnConnectionFailedListener  {

    private Location mLastLocation;
    Boolean mRequestingLocationUpdates;
    LocationManager locationManager;
    GoogleApiClient mGoogleApiClient;
    public static final int MY_PERMISSIONS_REQUEST = 0;
    Button get_Location;
    DateFormat dateFormat;
    String time, new_time;
    DatabaseHelper db;
    Calendar updateTime;
    LocationRequest locationRequest;
    TextView lat_text, lon_text, show_time;
    private int mInterval = 5000;
    Timer timer;

    Timer t;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.getlocation);

        timer = new Timer();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DatabaseHelper(this);

        //1 min= 60000ms , 1s=1000ms ,

        // locationRequest.setInterval(180000);  //i.e 3 minutes  ...3*60*1000


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        updateTime = Calendar.getInstance();
        updateTime.add(Calendar.MINUTE, 1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("kk:mm:ss");
        //the updated time is saved in the new_time variable
        new_time = simpleDateFormat.format(updateTime.getTime());


        lat_text = (TextView) findViewById(R.id.lattext);
        lon_text = (TextView) findViewById(R.id.lontext);
        show_time = (TextView) findViewById(R.id.display_time);
        Button location_data = (Button) findViewById(R.id.insert_loc_data);

        Calendar cal = Calendar.getInstance();
        Calendar.getInstance();

        //use handler to call a method periodically


        //Minutes added to the current time
        //cal.add(Calendar.MINUTE,15);

        SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");
        time = sdf.format(cal.getTime());

        show_time.setText(time + "");

        //updated time
        TextView update_time_txt = (TextView) findViewById(R.id.update_time);
        update_time_txt.setText(new_time + "");
        //update time in variable new_time and current time in variable time


        //Insert data into Location_updates table
        //insertLocationdata is the method, call it every 15mins
        //try for 1min i.e 60000, 1min
        location_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(!(lat_text.getText().toString().equals("") && lon_text.getText().toString().equals("")) ) {
                        Data();
                    }
                else
                    {
                        Toast.makeText(getApplicationContext(),"Latitute and Longitude is not being fetched",Toast.LENGTH_LONG).show();
                    }
            }
        });


        get_Location = (Button) findViewById(R.id.get_location);

       get_Location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //Communicating with the UI thread
                        GetLocation.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getLocation();
                            }
                        });
                    }

                }).start();
            }
        });

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                GetLocation.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Data();
                    }
                });
            }
        }).start();*/


        //Connecting to google play services
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    //.addConnectionCallbacks(this)
                    //.addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        //  timer.schedule(doTask,50000)
        //Repeating the task every few mins
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(!(lat_text.getText().toString().equals("") && lon_text.getText().toString().equals(""))) {

                    GetLocation.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Data();
                        }
                    });
                }
            }

        }, 10000, 500000); //10000ms=1s //Will be executed after 50 secs
    }

    //Method 1:

    /*Runnable runnable=new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(runnable,mInterval);
        }
    };*/


    //Method 2:

    /*TimerTask doTask=new TimerTask() {
        @Override
        public void run() {
      mHandler.post(new Runnable() {
          @Override
          public void run() {
              Data();
          }

      });
        }
    };*/

    //Method 3


    public void Data() {
        //runnable.run();
        boolean result = db.insertLocationData(lat_text.getText().toString(), lon_text.getText().toString(), show_time.getText().toString());
        if (result == true) {

            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
        }

    }


    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        return true;
    }


    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();

    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }

    public void getLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))

        {
            showSettingsAlert();
        }
        TextView lat = (TextView) findViewById(R.id.lattext);
        TextView lon = (TextView) findViewById(R.id.lontext);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //void requestPermissions (Activity activity,String[] permissions,int requestCode)

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST);
            }
            //ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  } ,0 );
            return;
        }


        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient); //The getLastLocation() method returns a Location object from which you can retrieve the latitude and longitude coordinates of a geographic location.
        if (mLastLocation != null) {
            lat.setText(String.valueOf(mLastLocation.getLatitude()));
            lon.setText(String.valueOf(mLastLocation.getLongitude()));
        }

    }

    /*@Override
    public void onConnected(@Nullable Bundle bundle) {
    //   if (mRequestingLocationUpdates) {
    //          startLocationUpdates();
    //    }
       locationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))

        {
            showSettingsAlert();
        }
        TextView lat = (TextView) findViewById(R.id.lattext);
        TextView lon = (TextView) findViewById(R.id.lontext);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //void requestPermissions (Activity activity,String[] permissions,int requestCode)

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST);
            }
            //ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  } ,0 );
            return;
        }


        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient); //The getLastLocation() method returns a Location object from which you can retrieve the latitude and longitude coordinates of a geographic location.
        if (mLastLocation != null) {
            lat.setText(String.valueOf(mLastLocation.getLatitude()));
            lon.setText(String.valueOf(mLastLocation.getLongitude()));
        }

    }*/

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(GetLocation.this);

        alertDialog.setTitle("Enable Gps");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //Explicitily calling the intent service to turn on the gps
                startActivity(intent);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
   /* @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }*/


    }
