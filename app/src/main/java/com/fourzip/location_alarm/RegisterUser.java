package com.fourzip.location_alarm;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.fourzip.location_alarm.View.FloatingTextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by NAISA_JOHN on 11/21/2016.
 */
public class RegisterUser extends AppCompatActivity {
    FloatingTextView name;
    FloatingTextView user_password;
    FloatingTextView email_id;
    FloatingTextView company_name;
    FloatingTextView phone_no;
    String get_email;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_page);


       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Registration:");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        //initialize the FirebaseAuth instance
        auth=FirebaseAuth.getInstance();

        //Get references of all the fields
      name=(FloatingTextView) findViewById(R.id.name);
         user_password=(FloatingTextView) findViewById(R.id.password);
         email_id=(FloatingTextView) findViewById(R.id.email_id);
        company_name=(FloatingTextView)findViewById(R.id.company_name);
        phone_no=(FloatingTextView)findViewById(R.id.phone_no);


        clearRegUserTxts();
        Button onCreateAccount=(Button)findViewById(R.id.createaccount);
        onCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email, password;
                email=email_id.getText().toString();
                password=user_password.getText().toString();

                get_email=email_id.getText().toString();
                    //Validation
                if(name.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Please fill name field",Toast.LENGTH_LONG).show();
                    return;
                }
                if(email_id.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Please fill Email-ID field",Toast.LENGTH_LONG).show();
                    return;
                }


                if(isValidEmail(get_email)==false)
                {
                    email_id.setError("Invalid Email");
                    return;
                }

                if(user_password.getText().length()==0)
                {
                    Toast.makeText(getApplicationContext(),"Please fill Password field",Toast.LENGTH_LONG).show();
                    return;
                }
                if(user_password.getText().length()<6)
                {
                    Toast.makeText(getApplicationContext(),"Password should be of minimum 8 characters",Toast.LENGTH_LONG).show();
                }
                if(name.getText().toString().equals("") && email_id.getText().toString().equals("") && user_password.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Please fill all the field",Toast.LENGTH_LONG).show();
                    return;

                }
                if(email_id.getText().toString().equals("") && user_password.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Required Email-ID and Password field",Toast.LENGTH_LONG).show();
                    return;
                }
                if(name.getText().toString().equals("") && user_password.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Required Name and Password field",Toast.LENGTH_LONG).show();
                    return;
                }
                if(phone_no.getText().length()==0)
                {
                    phone_no.setError("Field cannot be blank");
                }
                if(company_name.getText().length()==0)
                {
                    company_name.setError("Field cannot be blank");
                }

                //Firebase auth createAccount method which takes in email address and password,validates them
                auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(RegisterUser.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(RegisterUser.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            if (!task.isSuccessful()){
                                Toast.makeText(getApplicationContext(),"Authentication Failed",Toast.LENGTH_LONG).show();
                            }
                            else
                        {
                            Intent intent=new Intent(RegisterUser.this,MainActivity.class);
                            startActivity(intent);
                        }
                    }
                    });


                    //Toast.makeText(getApplicationContext(),"Account Created",Toast.LENGTH_LONG).show();
               /*AlertDialog.Builder builder=new AlertDialog.Builder(RegisterUser.this);
                builder.setMessage(R.string.dialog_message)
                        .setTitle(R.string.dialog_title);



                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        //jump to add reminder or fetch reminder activity
                        dialog.cancel();
                        Intent intent=new Intent(RegisterUser.this,AddReminder.class);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                        Intent intent=new Intent(RegisterUser.this,AddReminder.class);
                        startActivity(intent);
                    }
                });
                AlertDialog dialog = builder.create();

                 dialog.show();*/

                }



        });


        user_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    user_password.setHint("Password should be of atleast 8 characters");
                    return;
                }
            }
        });
        user_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    if(user_password.getText().length()==0) {
                        {
                            user_password.setError("Field cannot be left blank");
                            return;
                        }
                    }
            }
        });
        email_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                    if(email_id.getText().length()==0) {
                        {
                            email_id.setError("Field cannot be left blank");
                            return;
                        }
                    }
            }
        });
        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    name.setError("Field cannot be left blank");
                    return;
                }
            }
        });
    }

    public static boolean isValidEmail(CharSequence email)
    {
        if(TextUtils.isEmpty(email))
        {
            return false;
        }
        else
        {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public void onSaveData(View v)
    {
       // AlertDialog alertDialog=new AlertDialog();
        Toast msg=Toast.makeText(getApplicationContext(),"User Created",Toast.LENGTH_SHORT);
        msg.show();
        Intent intent=new Intent(RegisterUser.this,TaskActivity.class);
        startActivity(intent);
    }


    public class MyTextView extends TextView
    {

        public MyTextView(Context context) {
            super(context);
            init();
        }

        public MyTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init();
        }


        private void init() {
            if (!isInEditMode()) {
                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
                setTypeface(tf);
            }
        }



    }
    public void clearRegUserTxts()
    {
        name.setText("");
        user_password.setText("");
        company_name.setText("");
        phone_no.setText("");
        email_id.setText("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        clearRegUserTxts();
    }
}

