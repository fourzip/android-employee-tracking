package com.fourzip.location_alarm.model;

/**
 * Created by NAISA_JOHN on 2/22/2017.
 */
// The DataModel is used to retrieve the data for each CardView through getters.
public class RowItem {
    private String task_name;
    private String task_date;
    private String task_time;

    public RowItem(String name,String id,String email)
    {
        this.task_name=name;
        this.task_date=id;
        this.task_time=email;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getTask_date() {
        return task_date;
    }

    public void setTask_date(String task_date) {
        this.task_date = task_date;
    }

    public String getTask_time() {
        return task_time;
    }

    public void setTask_time(String task_time) {
        this.task_time = task_time;
    }


}
